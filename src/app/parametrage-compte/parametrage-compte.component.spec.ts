import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametrageCompteComponent } from './parametrage-compte.component';

describe('ParametrageCompteComponent', () => {
  let component: ParametrageCompteComponent;
  let fixture: ComponentFixture<ParametrageCompteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParametrageCompteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametrageCompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
