import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuhentificationComponent } from './auhentification.component';

describe('AuhentificationComponent', () => {
  let component: AuhentificationComponent;
  let fixture: ComponentFixture<AuhentificationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuhentificationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuhentificationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
