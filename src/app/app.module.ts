import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuhentificationComponent } from './auhentification/auhentification.component';
import { InscriptionComponent } from './inscription/inscription.component';
import { GestionEtablisementComponent } from './gestion-etablisement/gestion-etablisement.component';
import { ParametrageCompteComponent } from './parametrage-compte/parametrage-compte.component';
import { GestionCandidatsComponent } from './gestion-candidats/gestion-candidats.component';

@NgModule({
  declarations: [
    AppComponent,
    AuhentificationComponent,
    InscriptionComponent,
    GestionEtablisementComponent,
    ParametrageCompteComponent,
    GestionCandidatsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
